/*
   MultiBuilder - A build utility to automate building projects
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::path::PathBuf;

use crate::languagedefinition::LanguageDefinition;
use crate::logger::Logger;
use crate::paths;

#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct ConfigFile {
    pub languages: HashMap<String, LanguageDefinition>,
    pub verbose: Option<bool>,
}

impl ConfigFile {
    pub fn is_verbose(&self) -> bool {
        match self.verbose {
            Some(ref verbose) => *verbose,
            _ => false,
        }
    }

    /// Takes a path given to it, and attempt to guess the language type based on the files in this
    /// path, or files in the parent path[s] if unable to determine based on current path
    pub fn guess_language_type(
        &self,
        path: &PathBuf,
        verbose: bool,
        logger: &mut dyn Logger,
    ) -> Option<&LanguageDefinition> {
        let mut root_path = path.clone();
        canonicalize!(root_path);

        for path in root_path.ancestors() {
            for (language_name, language) in self.languages.iter() {
                // Attempt to guess based on finding project file in root path:
                for file_name in language.files.iter() {
                    let mut test_path = path.to_path_buf();
                    test_path.push(file_name);
                    if test_path.exists() {
                        if verbose {
                            logger.success(&"Language: ", language_name);
                        }
                        return Some(language);
                    }
                }
                // Attempt to guess language based on most-common file extention of current
                // directory:
                let top_exts = paths::get_normalized_file_extensions(path.to_path_buf());
                if let Some(language_extensions) = &language.extensions {
                    for ext in language_extensions.iter() {
                        for cmp_ext in top_exts.iter() {
                            if cmp_ext == ext {
                                logger.success(&"Found by extension: ", language_name);
                                return Some(language);
                            }
                        }
                    }
                }
            }
        }

        None
    }
}

/// Attempts to locate the first viable configuration file in the given path and load it. Returns a loaded instance of
/// Ok(ConfigFile) if successful, Err otherwise.
pub fn load_config_file(mut config_dir: PathBuf) -> Result<ConfigFile, String> {
    config_dir.push("config");
    match paths::load_from_file::<ConfigFile>(&config_dir) {
        Ok(config) => Ok(config),
        _ => Err("Unable to locate configuration file".to_owned()),
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::path::PathBuf;

    #[test]
    fn test_load_config_file() {
        use crate::builddefinition::BuildDefinition;
        use crate::languagedefinition::LanguageDefinition;

        struct TestCase<'a> {
            name: &'a str,
            config_path: PathBuf,
            expect: Result<ConfigFile, String>,
        }

        let valid_config: ConfigFile = ConfigFile {
            verbose: Some(true),
            languages: hashmap! {
                "rust".to_owned() => LanguageDefinition {
                    files: vec!["Cargo.toml".to_owned()],
                    extensions: Some(vec!["rs".to_owned()]),
                    globs: None,
                    check_subdirs: None,
                    targets:
                        hashmap!{
                            "build".to_owned() => BuildDefinition {
                                cmd: "cargo".to_owned(),
                                args: Some(vec!["build".to_owned()])
                            },
                            "test".to_owned() => BuildDefinition {
                                cmd: "cargo".to_owned(),
                                args: Some(vec!["test".to_owned()])
                            }
                        },
                },
                "makefile".to_owned() => LanguageDefinition {
                    files: vec!["Makefile".to_owned()],
                    extensions: Some(vec!["c".to_owned(), "cpp".to_owned(), "cxx".to_owned()]),
                    globs: None,
                    check_subdirs: None,
                    targets:
                        hashmap!{
                            "build".to_owned() => BuildDefinition {
                                cmd: "make".to_owned(),
                                args: None,
                            }
                        },
                }
            },
        };

        let test_cases: Vec<TestCase> = vec![
            TestCase {
                name: &"valid_json",
                config_path: [".", "tests", "configs", "valid", "json"].iter().collect(),
                expect: Ok(valid_config.clone()),
            },
            TestCase {
                name: &"valid_yaml",
                config_path: [".", "tests", "configs", "valid", "yaml"].iter().collect(),
                expect: Ok(valid_config.clone()),
            },
            TestCase {
                name: &"valid_ron",
                config_path: [".", "tests", "configs", "valid", "ron"].iter().collect(),
                expect: Ok(valid_config.clone()),
            },
        ];

        for test in test_cases {
            let result = load_config_file(test.config_path.clone());
            assert_eq!(
                result, test.expect,
                "{}\n\tWANTED:\t{:#?}\n\tGOT:\t{:#?}\n\tCONFIG_PATH:\t{:?}\n",
                test.name, test.expect, result, test.config_path
            );
        }
    }
}
