/*
   MultiBuilder - A build utility to automate building projects
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Default, Debug, Clone, Serialize, Deserialize, PartialEq)]
pub struct BuildDefinition {
    pub cmd: String,
    pub args: Option<Vec<String>>,
}

impl BuildDefinition {
    /// Returns a vector that copies the current build instructions from the builder, replacing any
    /// delimited values with appropriate workspace values provided.
    pub fn get_cmd(&self) -> Vec<String> {
        let mut result = Vec::new();
        let environment = HashMap::new();

        result.push(resolve_variables(&self.cmd, &environment));

        if let Some(ref args) = self.args.as_ref() {
            for arg in args.into_iter() {
                result.push(resolve_variables(arg, &environment));
            }
        }

        result
    }
}

fn resolve_variables(arg: &str, environment: &HashMap<&str, &str>) -> String {
    let mut result = String::new();
    let mut chars = arg.char_indices();

    while let Some((_idx, chr)) = chars.next() {
        // If we parse a variable, get the variable name and replace it if we have it:
        if chr == '{' {
            let mut var_name = String::new();

            while let Some(next) = chars.next() {
                if next.1 == '}' {
                    // We have a closing character; attempt to match in environment
                    match &environment.get(var_name.as_str()) {
                        Some(ref matched) => {
                            var_name.clear();
                            matched.char_indices().for_each(|ch| result.push(ch.1));
                            break;
                        }
                        _ => {
                            // No matching "environment" variable found; add text unaltered with
                            // surrounding brackets
                            result.push('{');
                            var_name.char_indices().for_each(|ch| result.push(ch.1));
                            result.push('}');
                            var_name.clear();
                            break;
                        }
                    }
                } else {
                    var_name.push(next.1);
                }
            }

            if var_name.len() > 0 {
                // No match was found AND we didn't find a closure; add variable name and opening
                // bracket:
                result.push('{');
                var_name.char_indices().for_each(|ch| result.push(ch.1));
            }
        } else {
            result.push(chr);

            // If next character is escaped AND we have another character, we add the unescaped
            // version:
            // TODO: Is this actually what we want?
            if chr == '\\' {
                if let Some(next) = chars.next() {
                    result.push(next.1);
                } else {
                    result.push(chr);
                }
            }
        }
    }

    result
}

#[cfg(test)]
mod test {
    use super::*;
    use std::collections::HashMap;

    #[test]
    fn test_resolve_variables() {
        struct TestCase {
            name: &'static str,
            arg: &'static str,
            environment: HashMap<&'static str, &'static str>,
            expect: &'static str,
        }
        let test_cases = vec![
            TestCase {
                name: &"arg_without_match",
                arg: &"hi {unmatched}",
                environment: hashmap! {"matched" => "replaced_match"},
                expect: &"hi {unmatched}",
            },
            TestCase {
                name: &"escaped",
                arg: &"hi \\{unmatched}",
                environment: hashmap! {"matched" => "replaced_match"},
                expect: &"hi \\{unmatched}",
            },
            TestCase {
                name: &"arg_with_match",
                arg: &"hi {matched}",
                environment: hashmap! {"matched" => "replaced_match"},
                expect: &"hi replaced_match",
            },
            TestCase {
                name: &"arg_with_matches",
                arg: &"hi {matched} {a}",
                environment: hashmap! {"matched" => "replaced_match", "a" => "match"},
                expect: &"hi replaced_match match",
            },
            TestCase {
                name: &"arg_with_match_and_unmatched",
                arg: &"hi {matched}{unmatched}{unmatched}{a}",
                environment: hashmap! {"matched" => "replaced_match", "a" => "match"},
                expect: &"hi replaced_match{unmatched}{unmatched}match",
            },
            TestCase {
                name: &"arg_unmatched_opening",
                arg: &"hi {matched",
                environment: hashmap! {"matched" => "replaced_match", "a" => "match"},
                expect: &"hi {matched",
            },
        ];

        for test_case in test_cases {
            let result = resolve_variables(&test_case.arg, &test_case.environment);
            assert_eq!(test_case.expect, result, "{}", test_case.name);
        }
    }
}
