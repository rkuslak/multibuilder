use std::path::PathBuf;
use std::process::Command;

use crate::languagedefinition::LanguageDefinition;
use crate::logger::Logger;
use crate::projectfile::ProjectFile;

use clap::{App, Arg};

pub const DEFAULT_ERROR: i32 = !0;

pub struct CmdLineOptions {
    pub language: Option<String>,
    pub verbose: bool,
    pub relative_path: String,
    pub recursion: i16,
    pub build_target: String,
}

const VERSION_CMD: &str = "version";
const LANGUAGE_CMD: &str = "language";
const RELATIVE_CMD: &str = "relative";
const RECURSION_CMD: &str = "recursion";
const TARGET_CMD: &str = "target";
const VERBOSE_CMD: &str = "verbose";

/// Parses the passed command line options, displaying help text on error or if passed "-h" or
/// "--help", and returns a instance of CmdLineOptions containing the resolved options.
pub fn parse_cmd_line(_logger: &mut dyn Logger, args: &[String]) -> Result<CmdLineOptions, i32> {
    let app = App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .arg(
            Arg::with_name(RELATIVE_CMD)
                .long(RELATIVE_CMD)
                .short("s")
                .value_name("PATH_OR_FILE")
                .help("Specifies a path to begin searching relative to. If a file, uses the file's containing directory, otherwise searches from directory")
                .takes_value(true),
        )
        .arg(
            Arg::with_name(RECURSION_CMD)
                .long(RECURSION_CMD)
                .short("r")
                .value_name("LEVELS")
                .help("Specifies a path to begin searching relative to. If a file, uses the file's containing directory, otherwise searches from directory")
                .takes_value(true),
        )
        .arg(
            Arg::with_name(LANGUAGE_CMD)
                .long(LANGUAGE_CMD)
                .short("l")
                .value_name("LANGUAGE")
                .help("Specifies the language to use, overriding autodetection")
                .takes_value(true),
        )
        .arg(
            Arg::with_name(TARGET_CMD)
                .long(TARGET_CMD)
                .short("t")
                .value_name("BUILD_TARGET")
                .takes_value(true),
        )
        .arg(Arg::with_name(VERSION_CMD).long(VERSION_CMD))
        .arg(Arg::with_name(VERBOSE_CMD).short("v").long(VERBOSE_CMD).help("Turns on verbose output").multiple(true));
    let matches = match app.get_matches_from_safe(args) {
        Ok(matches) => matches,
        Err(e) => {
            println!("{}", e);
            return Err(DEFAULT_ERROR);
        }
    };

    if matches.is_present(VERSION_CMD) {
        println!("{}", crate_version!());
        return Err(0);
    }

    let recursion = match matches.value_of("recursion") {
        Some(recursion_str) => match recursion_str.parse::<i16>() {
            Ok(recusion) => recusion,
            _ => {
                println!("Unable to parse recusion option \"{}\"", recursion_str);
                return Err(DEFAULT_ERROR);
            }
        },
        _ => 0,
    };

    let language = match matches.value_of("language") {
        Some(l) => Some(l.to_string()),
        _ => None,
    };

    Ok(CmdLineOptions {
        verbose: matches.is_present("verbose"),
        recursion,
        relative_path: matches.value_of("relative").unwrap_or_else(|| &".").to_string(),
        language,
        build_target: matches.value_of("target").unwrap_or_else(|| &"build").to_string(),
    })
}

/// Runs the passed cmd with the passed arguments, and returns the resulting exit code of the
/// spawned process. If unable to spawn or other internal errors are encountered, returns -1.
pub fn run_build_cmd(cmds: &[String], working_dir: &PathBuf) -> Result<i32, String> {
    let mut cmds = cmds.iter();
    if let Some(cmd) = cmds.next() {
        let mut runner = Command::new(cmd);
        runner.current_dir(working_dir);

        for arg in cmds {
            runner.arg(arg);
        }
        let mut child = match runner.spawn() {
            Ok(child) => child,
            _ => {
                return Err("Run attempt failed".to_owned());
            }
        };

        return match child.wait() {
            Ok(status) => match status.code() {
                Some(code) => Ok(code),
                _ => Err("process killed by OS signal".to_owned()),
            },
            Err(e) => {
                let msg = format!("error spawning build process: {}", e);
                Err(msg)
            }
        };
    }

    return Err("no command passed".to_string());
}

/// Attempts to build the project for the passed build_target, attempting to resolve the target
/// first with the project_file (if provided), then with the language (if provided). If
/// successful returns the build processes exit code, otherwise returns -1.
pub fn build_project(
    project_file: Option<ProjectFile>,
    language: Option<&LanguageDefinition>,
    project_root: &std::path::PathBuf,
    build_target: &str,
    logger: &mut dyn Logger,
) -> i32 {
    let project_builder = match &project_file {
        Some(ref project_file) => project_file.get_builder(build_target),
        _ => None,
    };
    let config_builder = match &language {
        Some(ref language) => language.get_builder(build_target),
        _ => None,
    };

    let builder = match (project_builder, config_builder) {
        (Some(builder), _) => Some(builder),
        (_, Some(builder)) => Some(builder),
        _ => None,
    };

    let code: Result<i32, String> = match builder {
        Some(builder) => run_build_cmd(&builder.get_cmd(), &project_root),
        _ => Err(format!("Unable to find build target {}", build_target)),
    };

    match code {
        Ok(code) => code,
        Err(msg) => {
            logger.error(&msg);
            -1
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_run_build_cmd() {
        struct TestCase {
            name: &'static str,
            cmds: Vec<String>,
            expect_ok: bool,
            expect_code: i32,
        }
        let mut test_dir = std::path::PathBuf::from(".");
        test_dir.push("target/debug");

        let test_cases: Vec<TestCase> = vec![
            #[cfg(target_family = "windows")]
            TestCase {
                name: "successful_test",
                cmds: vec!["cmd".to_owned(), "/C".to_owned(), "dir".to_owned()],
                expect_ok: true,
                expect_code: 0,
            },
            #[cfg(target_family = "unix")]
            TestCase {
                name: "successful_test",
                cmds: vec!["ls".to_owned(), "-la".to_owned()],
                expect_ok: true,
                expect_code: 0,
            },
            TestCase {
                name: "failed_with_error",
                cmds: vec!["fake_cmd_name".to_owned()],
                expect_ok: false,
                expect_code: 0,
            },
        ];

        for test in test_cases.iter() {
            match run_build_cmd(&test.cmds, &test_dir) {
                Ok(code) => {
                    assert_eq!(
                        true, test.expect_ok,
                        "{}\n\tWANTED:\tErr(_)\n\tGOT:\tOk({})",
                        test.name, code
                    );
                    assert_eq!(
                        test.expect_code, code,
                        "{}\n\tWANTED:\t{}\n\tGOT:\t{}",
                        test.name, test.expect_code, code
                    );
                }
                Err(e) => {
                    assert_eq!(
                        false, test.expect_ok,
                        "{}\n\tWANTED:\tErr(_)\n\tGOT:\tErr({})",
                        test.name, e
                    );
                }
            }
        }
    }
}
