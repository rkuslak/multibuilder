/*
   MultiBuilder - A build utility to automate building projects
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use std::io::Write;
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};

pub struct ConsoleLogger {
    stdout: StandardStream,
    stderr: StandardStream,
    default_color: ColorSpec,
    success_color: ColorSpec,
    failure_color: ColorSpec,
}

pub trait Logger {
    fn success(&mut self, label: &str, result: &str);
    fn failure(&mut self, label: &str, result: &str);
    fn error(&mut self, msg: &str);
}

// TODO: This could be a lot cleaner, but works for leaving an abstraction framework for now. We're
// seriously just unwrapping everything? Current assumption is failure to write to std[out/err] is
// extreme enough a failure to want the panic behavior
impl ConsoleLogger {
    pub fn new() -> ConsoleLogger {
        let mut default_color = ColorSpec::new();
        default_color.set_fg(Some(Color::White)).set_bold(false);

        let mut success_color = ColorSpec::new();
        success_color.set_fg(Some(Color::Green)).set_bold(true);
        let mut failure_color = ColorSpec::new();
        failure_color.set_fg(Some(Color::Red)).set_bold(true);

        ConsoleLogger {
            stdout: StandardStream::stdout(ColorChoice::Auto),
            stderr: StandardStream::stderr(ColorChoice::Auto),
            default_color,
            success_color,
            failure_color,
        }
    }
}

impl Logger for ConsoleLogger {
    fn success(&mut self, label: &str, result: &str) {
        self.stdout.set_color(&self.default_color).unwrap();
        write!(self.stdout, "{}", label).unwrap();
        self.stdout.set_color(&self.success_color).unwrap();
        writeln!(self.stdout, "{}", result).unwrap();
        self.stdout.reset().unwrap();
    }

    fn failure(&mut self, label: &str, result: &str) {
        self.stdout.set_color(&self.default_color).unwrap();
        write!(self.stdout, "{}", label).unwrap();
        self.stdout.set_color(&self.failure_color).unwrap();
        writeln!(self.stdout, "{}", result).unwrap();
        self.stdout.reset().unwrap();
    }

    fn error(&mut self, msg: &str) {
        self.stderr.set_color(&self.failure_color).unwrap();
        writeln!(self.stderr, "{}", msg).unwrap();
        self.stderr.reset().unwrap();
    }
}
