use crate::builddefinition::BuildDefinition;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;

/// A struct to contain information derived from a project-specific configuration file held in a
/// "dot" file that is special to this application. Can contain information for building,
/// debugging, tests, etc..
#[derive(Serialize, Deserialize, Debug, Default, Clone, PartialEq)]
pub struct ProjectFile {
    pub builders: Option<HashMap<String, BuildDefinition>>,
}

impl ProjectFile {
    /// Attempts to locate the build target for the given string representation in the hash map.
    /// Notes search is case sensitive
    pub fn get_builder(&self, target: &str) -> Option<&BuildDefinition> {
        match self.builders.as_ref() {
            Some(ref builders) => match builders.get(target) {
                Some(ref builder) => Some(builder),
                _ => None,
            },
            _ => None,
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::default::Default;

    #[test]
    fn test_deserialize() {
        struct TestCase<'tc> {
            name: &'tc str,
            json: &'tc str,
            want: ProjectFile,
            want_err: bool,
        }
        let test_cases = vec![
            TestCase {
                name: "empty_test",
                json: "{}",
                want: Default::default(),
                want_err: false,
            },
            TestCase {
                name: "rust_builder",
                json: "{
                    \"builders\": {
                        \"rust\": {
                            \"cmd\": \"cargo\",
                            \"args\": [\"build\"]
                        }
                    }
                }",
                want: ProjectFile {
                    builders: Some(hashmap! {
                        "rust".to_owned() => BuildDefinition {
                            cmd: "cargo".to_owned(),
                            args: Some(vec![
                                "build".to_owned(),
                            ]),
                        }
                    }),
                },
                want_err: false,
            },
            TestCase {
                name: "partial_eq",
                json: "{
                    \"builders\": {
                        \"test\": {
                            \"cmd\": \"cargo\",
                            \"args\": [\"test\"]
                        },
                        \"build\": {
                            \"cmd\": \"cargo\",
                            \"args\": [\"build\"]
                        }
                    }
                }",
                want: ProjectFile {
                    builders: Some(hashmap! {
                        "build".to_owned() => BuildDefinition {
                            cmd: "cargo".to_owned(),
                            args: Some(vec![ "build".to_owned(), ]),
                        },
                        "test".to_owned() => BuildDefinition {
                            cmd: "cargo".to_owned(),
                            args: Some(vec![ "test".to_owned(), ]),
                        }
                    }),
                },
                want_err: false,
            },
        ];

        for test_case in test_cases {
            let decoder_result: Result<ProjectFile, serde_json::Error> = serde_json::from_str(test_case.json);
            match decoder_result {
                Ok(result) => {
                    assert!(!test_case.want_err, "Wanted: Err. Got: {:?}", result);
                    assert_eq!(
                        test_case.want, result,
                        "Name: {}, Wanted: {:#?}. Got: {:#?}",
                        test_case.name, test_case.want, result
                    );
                }
                Err(e) => {
                    assert!(test_case.want_err, "Wanted: {:?}. Got: {:?}", test_case.want, e);
                }
            }
        }
    }
}
