/*
   MultiBuilder - A build utility to automate building projects
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::path::PathBuf;

use crate::builddefinition::BuildDefinition;
use crate::logger::Logger;

#[derive(Debug, Clone, Default, Serialize, Deserialize, PartialEq)]
pub struct LanguageDefinition {
    pub targets: HashMap<String, BuildDefinition>,
    pub files: Vec<String>,
    pub globs: Option<Vec<String>>,
    pub extensions: Option<Vec<String>>,
    pub check_subdirs: Option<bool>,
}

/// A wrapper over the PathBuf type that declares what type of path is returned for later handling.
#[derive(Clone, PartialEq)]
pub enum PathType {
    ProjectRoot(PathBuf),
    VimProjectRoot(PathBuf, PathBuf),
}

impl std::convert::From<PathType> for String {
    fn from(path: PathType) -> String {
        match &path {
            _ => "".to_owned(),
        }
    }
}

impl std::fmt::Display for PathType {
    fn fmt(&self, out: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            PathType::ProjectRoot(ref path) => write!(out, "PathType::ProjectRoot({:?})", path),
            PathType::VimProjectRoot(ref project, ref path) => {
                write!(out, "PathType::VimProjectRoot({:?}, {:?})", project, path)
            }
        }
    }
}

impl LanguageDefinition {
    pub fn get_builder(&self, build_target: &str) -> Option<&BuildDefinition> {
        self.targets.get(build_target)
    }
}

/// Attempts to locate the given buildfile in any subdirectory of the passed root_path. If found,
/// returns Some(found_path_root), otherwise returns None
fn check_subdirs_for_file(mut root_path: PathBuf, buildfile: String) -> Option<PathType> {
    // get a iterator for all directories in path:
    let subdirs = root_path
        .read_dir()
        .unwrap()
        .filter(|path| path.is_ok())
        .map(|path| PathBuf::from(path.unwrap().path()))
        .filter(|path| path.is_dir());

    for ref subpath in subdirs {
        let mut test_path = root_path.clone();
        test_path.push(subpath);
        test_path.push(&buildfile);
        if test_path.exists() {
            root_path.push(subpath);
            return Some(PathType::ProjectRoot(root_path));
        }
    }
    None
}

/// Finds any of the build files for a given path and language. Returns the root path on
/// success, or None if no root build path can be found. If recursion_level is set to a
/// positive, non-0 value, it will recurse at most recursion_level times of finding a build
/// file before returning the last result found or None if no results.
pub fn find_project_root<'ld>(
    path: &PathBuf,
    verbose: bool,
    mut recursion_level: i16,
    logger: &mut dyn Logger,
    language: Option<&'ld LanguageDefinition>,
) -> Option<PathType> {
    let mut result: Option<PathType> = None;
    let mut root_path = PathBuf::from(path);

    canonicalize!(root_path);

    // Create PathBuf we can add to test for project files
    let project_file: PathBuf = [".project", "config.json"].iter().collect();

    for path in root_path.ancestors() {
        log_path!(logger, verbose, "Checking ", path);
        let project_root = PathBuf::from(path);

        let buildfile: Option<PathType> = {
            // Attempt to find build instructions in the ".project" directory, if present
            let mut project_dir = project_root.clone();
            project_dir.push(&project_file);
            if project_dir.exists() {
                return Some(PathType::VimProjectRoot(project_root, project_dir));
            }

            let (language_files, check_subdirs) = match language {
                Some(ref language) => (language.files.iter(), language.check_subdirs),
                _ => ([].iter(), None),
            };

            // Attempt to find based on Buildfile name:
            for buildfile in language_files {
                let mut test_path = project_root.clone();
                test_path.push(buildfile);
                if test_path.exists() {
                    return Some(PathType::ProjectRoot(PathBuf::from(path)));
                }

                if !check_subdirs.unwrap_or_default() {
                    continue;
                }

                // If no match is found AND language wants to check subdirs, do so:
                if let Some(found_root) = check_subdirs_for_file(project_root.clone(), buildfile.to_string()) {
                    return Some(found_root);
                }
            }

            None
        };

        if let Some(path) = buildfile {
            recursion_level -= 1;
            if recursion_level == 0 {
                log_pathtype!(logger, verbose, "Project found: ", path);
                return Some(path);
            }

            result = Some(path);
        }
    }

    // If we haven't returned early due to recursion limit, return now:
    if let Some(ref path) = result {
        log_pathtype!(logger, verbose, "Project found: ", path);
    }
    result
}

mod test {}
