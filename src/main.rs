/*
   MultiBuilder - A build utility to automate building projects
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#[macro_use]
extern crate clap;
extern crate dirs;
extern crate ron;
extern crate serde;
extern crate serde_json;
extern crate serde_yaml;
extern crate termcolor;
extern crate toml;

#[cfg(taget_family = "windows")]
extern crate dunce;

#[macro_use]
mod macros;

mod cmdline;
mod logger;
mod paths;

mod builddefinition;
mod configfile;
mod languagedefinition;
mod projectfile;

use crate::logger::Logger;
use dirs::config_dir;
use projectfile::ProjectFile;

/// A macro that provides a quick, easy means to log an error and then exit the
/// program. This will cause the application to return the passed "code" as the
/// exit code for the program.
macro_rules! msg_exit {
    ($logger: ident, $msg: expr, $code: expr) => {
        $logger.error($msg);
        std::mem::drop(&mut $logger);
        std::process::exit(($code).into());
    };
}

fn main() {
    let mut logger = logger::ConsoleLogger::new();
    let mut config_dir = config_dir().unwrap();
    config_dir.push("multibuilder");
    let config_file = match configfile::load_config_file(config_dir.clone()) {
        Ok(config_file) => config_file,
        Err(_) => Default::default(),
    };

    let args: Vec<String> = std::env::args().collect();
    let options = match cmdline::parse_cmd_line(&mut logger, &args) {
        Ok(options) => options,
        Err(err_code) => {
            msg_exit!(logger, "Unable to continue with invalid command line option", err_code);
        }
    };

    let verbose = options.verbose || config_file.is_verbose();
    let recursion = options.recursion;
    let mut search_path = std::path::PathBuf::from(&options.relative_path)
        .canonicalize()
        .ok()
        .expect("Failed to get starting directory path");

    // TODO: If we're building a specific file, use it? currently find directory
    // housing file as we only build the full project.
    if !search_path.is_dir() {
        search_path.pop();
    }

    let language = match options.language {
        Some(language_name) => config_file.languages.get(&language_name),
        _ => config_file.guess_language_type(&search_path, verbose, &mut logger),
    };

    use languagedefinition::{find_project_root, PathType};

    let project = find_project_root(&search_path, verbose, recursion, &mut logger, language);
    let (project_file, project_root): (Option<ProjectFile>, std::path::PathBuf) = match project {
        Some(PathType::ProjectRoot(build_path)) => (None, build_path),
        Some(PathType::VimProjectRoot(root_path, project_file_path)) => {
            let project_file = paths::load_from_file::<ProjectFile>(&project_file_path);

            if let Err(e) = &project_file {
                logger.error(&e);
            };

            (project_file.ok(), root_path)
        }
        _ => {
            msg_exit!(logger, "Failed to find project to build", !0);
        }
    };

    match cmdline::build_project(
        project_file,
        language,
        &project_root,
        &options.build_target,
        &mut logger,
    ) {
        code if code != 0 => {
            std::mem::drop(&mut logger);
            std::process::exit(cmdline::DEFAULT_ERROR);
        }
        _ => (),
    }
}
