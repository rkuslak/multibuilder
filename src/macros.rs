#[macro_export]
macro_rules! log_path {
    ($logger: ident, $verbose: expr, $header: expr, $path: ident) => {
        if $verbose {
            match $path.to_str() {
                Some(path) => $logger.success($header, path),
                _ => $logger.success($header, &$path.to_string_lossy()),
            }
        }
    };
}

#[macro_export]
macro_rules! log_pathtype {
    ($logger: ident, $verbose: expr, $header: expr, $path: ident) => {
        if $verbose {
            let path_fmt = format!("{}", $path);
            $logger.success($header, &path_fmt);
        }
    };
}

#[macro_export]
#[cfg(target_family = "windows")]
macro_rules! canonicalize {
    ($file_path:ident) => {
        if !$file_path.is_absolute() {
            $file_path = dunce::canonicalize($file_path).ok()?;
        }
    };
}

#[macro_export]
#[cfg(not(target_family = "windows"))]
macro_rules! canonicalize {
    ($file_path:ident) => {
        if !$file_path.is_absolute() {
            $file_path = std::fs::canonicalize($file_path).ok()?;
        }
    };
}

#[macro_export]
macro_rules! hashmap (
    { $($key:expr => $val:expr),+ } => {
        {
            let mut map = std::collections::HashMap::new();
            $( map.insert($key, $val); )+
            map
        }
    }
);
