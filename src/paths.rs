/*
   MultiBuilder - A build utility to automate building projects
   Copyright (C) 2019 Ron Kuslak
   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use std::collections::HashMap;
use std::default::Default;
use std::path::PathBuf;

/// Parses the directory pointed to by path, and notes the extension of the files contained
/// therein. It then returns a Vec of these extensions, sorted by most common to least and then
/// alphabetical within matching occurrance levels.
pub fn get_normalized_file_extensions(path: PathBuf) -> Vec<String> {
    let mut results: Vec<String> = Vec::new();
    let mut results_sizes: Vec<u64> = Default::default();

    if let Ok(files_iter) = path.read_dir() {
        let mut weighted_extensions: HashMap<String, u64> = HashMap::new();

        // Let's get crazy with some iterator functions to gather all file extensions:
        for ext in files_iter
            .filter_map(|opt_file| opt_file.ok())
            .map(|dir_entry| dir_entry.path())
            .filter(|path| path.is_file())
            .filter_map(|path| path.extension().and_then(|p| Some(p.to_os_string())))
            .filter_map(|ext| ext.into_string().ok())
        {
            match weighted_extensions.get_mut(&ext) {
                Some(value) => *value += 1,
                _ => {
                    weighted_extensions.insert(ext.to_string(), 1);
                }
            };
        }

        let ext_iter = weighted_extensions.iter();
        for (ext_ptr, weight) in ext_iter {
            results_sizes.push(*weight);
            let ext = String::from((*ext_ptr).as_str());
            results.push(ext);
            // Sort by result size OR if equal, sort by string order:
            for cmp_iter in (1..(results.len())).rev() {
                if results_sizes[cmp_iter] < results_sizes[cmp_iter - 1]
                    || (results_sizes[cmp_iter] == results_sizes[cmp_iter - 1]
                        && results[cmp_iter] > results[cmp_iter - 1])
                {
                    break;
                }
                results.swap(cmp_iter, cmp_iter - 1);
                results_sizes.swap(cmp_iter, cmp_iter - 1);
            }
        }
    };
    results
}

/// A Result alias for the results returned from load_from_file:
pub type FileResult<T> = Result<T, String>;

pub type DeserializerFunction<T> = fn(reader: std::io::BufReader<std::fs::File>) -> FileResult<T>;

/// A general function to attempt loading various files and deserializing using Serde into a valid
/// object. Will guess the file type based on the extension of the file. Supports deserizing JSON
/// and YAML files. On failure, returns a wrapper of the error encountered of a String describing
/// the issue encountered.
pub fn load_from_file<T>(filename: &PathBuf) -> FileResult<T>
where
    T: for<'de> serde::Deserialize<'de>,
{
    use std::fs::File;
    use std::io::BufReader;

    let mut valid_exts: HashMap<&'static str, DeserializerFunction<T>> = HashMap::new();
    valid_exts.insert(&"json", |reader| {
        serde_json::from_reader(reader).map_err(|e| e.to_string())
    });
    valid_exts.insert(&"yaml", |reader| {
        serde_yaml::from_reader(reader).map_err(|e| e.to_string())
    });
    valid_exts.insert(&"yml", |reader| {
        serde_yaml::from_reader(reader).map_err(|e| e.to_string())
    });
    valid_exts.insert(&"ron", |reader| ron::de::from_reader(reader).map_err(|e| e.to_string()));

    for (extention, decoder) in valid_exts {
        let mut full_path: PathBuf = filename.clone();
        full_path.set_extension(extention);
        if full_path.exists() {
            let project_file = File::open(&full_path).map_err(|e| e.to_string())?;
            let reader = BufReader::new(project_file);
            match decoder(reader) {
                Ok(config_file) => return Ok(config_file),
                Err(e) => return Err(e),
            };
        }
    }

    Err("Project path is invalid".to_owned())
}

#[cfg(test)]
mod test {
    use std::path::PathBuf;

    #[test]
    fn test_get_normalized_file_extensions() {
        // find project root
        let mut root_path = PathBuf::from(".");
        root_path = root_path.canonicalize().ok().expect("Failed to get root path");
        root_path.push("tests");
        root_path.push("extensions");

        struct TestCase {
            name: String,
            expect: Vec<String>,
            subdir: String,
        }

        let test_cases: Vec<TestCase> = vec![
            TestCase {
                // 'b' is most requent, results should break alphabetical order
                name: "ORDERED_TEST".to_owned(),
                expect: vec![
                    "b".to_owned(),
                    "1".to_owned(),
                    "2".to_owned(),
                    "3".to_owned(),
                    "a".to_owned(),
                    "c".to_owned(),
                ],
                subdir: "ordered_result".to_owned(),
            },
            TestCase {
                name: "OCCURANCE_ORDERED".to_owned(),
                expect: vec!["cpp".to_owned(), "h".to_owned()],
                subdir: "multiple_of_same".to_owned(),
            },
        ];

        for test in test_cases.iter() {
            let mut path = root_path.clone();
            path.push(&test.subdir);
            let result = super::get_normalized_file_extensions(path);
            assert_eq!(result.len(), test.expect.len());
            for x in 0..result.len() {
                assert_eq!(
                    result[x], test.expect[x],
                    "{} FAILED:\n\tHAD:\t{:?}\n\tWANTED:\t{:?}",
                    test.name, result, test.expect
                );
            }
        }
    }
}
