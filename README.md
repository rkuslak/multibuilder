MultiBuilder
===

# Overview
MultiBuilder is a simple program intended to find the most local to the current
directory project file (Cargo.toml, go.mod, projectName.sln, Makefile, etc) and
attempt to invoke it. Support is available for custom running commands. This
mainly came out of the fact that fairly often I will want to build a project
when in a directory that is not the root from Neovim, but don't feel like
adjusting the make command each time I change projects or even change between
submodules/subcrates within a project and would prefer to just have a constant
muscle memory binding to instantly fire off a build.

It is in no way meant to be a replacement for a proper build tool like make,
cargo, cmake, etc., but rather to make it easier to spawn those tools without
having to think about where you are currently within a build directory, for
example if you are calling it from a [Neo]Vim or Emacs keymap or via a VSCode
build task.

# Usage
You can normally kick off a build simply by running the `multibuilder` command
for any directory under your source tree. For full options, run `multibuilder -?`.
You can specify the specific of a given language in the "config.json" file,
which should be found in `${XDG_CONFIG_HOME}/multibuilder/config.json`
(defaulting to `${HOME}/.config` if not defined). See the included config.json
for a example of how to configure the file, as this will touch on all available
options.

## Project Configuration Files

In addition to the applications configuration file, you can specify unique
building targets via creating a `.project/config.json` file within a projects
root directory. In this file you can define custom build targets the same as you
would in the main configuration file without needing to define a language type
or discovery scheme. Additionally you can use this to redefine global build
commands that have been defined in the applications configuration, should a
custom target for a certain project be needed (for example, redefining the
`build` target in the project to add specical command line options only required
for that project whereas generalized builds do not require them).

The source includes a sample config.json for a global layout that should be a
good general option. The layout of the per-project `config.json` for a
individual project is as follows:

```json
{
    "builders": {
        "Build Target Name": {
            "cmd": "builder",
            "args": ["command", "line", "arguments"]
        }
    }
}
```

## Command Line Arguments

```
Options:
    -l, --language [LANGUAGE]
                        Specifies the language to use, overriding
                        autodetection
    -v, --verbose       Turns on verbose output
    -h, --help          Shows this help dialog
    -s, --relative [PATH]
                        Specifies a path to begin searching relative to. If a
                        file, uses the file's containing directory, otherwise
                        searches from directory
    -r, --recursion [NUMBER_OF_LEVELS]
                        specifies the levels of recursion to search for build
                        files before running task. Runs top most recursive
                        found or last target if amount specified can not be
                        found
    -t, --target [BUILD_TARGET]
                        Specifies the "build target" to be run once root
                        directory and language is defined. If not specified,
                        defaults to "build"
```

# Installation
## Building

To build this you will need a copy of Rust available supporting the 2018
standard. Run `cargo build` to build, then copy the executable into a folder
available from your path. If you have access to Golang and no means to compile a
Rust project, a older version of the codebase in Golang is available in the
pre-0.5 tags.

## [Neo]Vim

As I spend most of my time in Neovim, this was designed to be easy to fit into
this work flow. Simply place the executable in any directory in your path, and
then add a line similar to this to build the project connected to the currently
open buffer:

```
map <leader>bb <esc>:split term://multibuilder -v -t build -s %<cr>
map <leader>bt <esc>:split term://multibuilder -v -t test -s %<cr>
```

You can set it to load based on the file type of the currently edited file as
well, with something similar to this:

```
map <leader>bb <esc>:exec ':split term://multibuilder -v -t build -l ' . &filetype<cr>
map <leader>bt <esc>:exec ':split term://multibuilder -v -t test -l ' . &filetype<cr>
```

# Road Map
## TODOs
* Some deeper logic for finding projects such as Cmake projects where build
  "project" buildfiles may reside in a subdirectory of the target.
* More "intelligent" detection of current directories project
* Possibly a TUI debugger client for Debug Adaptor Protocol providers
